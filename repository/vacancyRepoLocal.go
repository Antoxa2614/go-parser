package repository

import (
	"fmt"
	"gitlab.com/antoxa2614/go-parser/model"
	"strconv"
)

type VacanStorage struct {
	data map[string]*model.Vacancy
}

func NewVacanStorage() *VacanStorage {
	return &VacanStorage{data: map[string]*model.Vacancy{}}

}

func (s *VacanStorage) Create(vac model.Vacancy) error {
	if _, ok := s.data[vac.ID]; ok {
		return fmt.Errorf("Вакансия с таким ID есть в списке")
	} else {
		s.data[vac.ID] = &vac
	}
	return nil

}

func (s *VacanStorage) GetBiId(id int) (model.Vacancy, error) {
	idStr := strconv.Itoa(id)
	if val, ok := s.data[idStr]; ok {
		return *val, nil

	}
	return model.Vacancy{}, fmt.Errorf("Вакансии с таким ID нет в списке")

}

func (s *VacanStorage) GetList() ([]model.Vacancy, error) {
	var slice []model.Vacancy

	for _, val := range s.data {
		slice = append(slice, *val)
	}
	if len(slice) < 1 {
		return []model.Vacancy{}, fmt.Errorf("Список вакансий пуст")
	}
	return slice, nil

}

func (s VacanStorage) Delete(id int) error {
	idStr := strconv.Itoa(id)
	if _, ok := s.data[idStr]; ok {
		delete(s.data, idStr)
		return nil

	}
	return fmt.Errorf("Вакансии с таким ID нет в списке")

}
