package repository

import (
	"context"
	"fmt"
	"gitlab.com/antoxa2614/go-parser/model"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

type StorageMongo struct {
	Db *mongo.Collection
}

func (s *StorageMongo) Create(vacancy model.Vacancy) error {
	insertResult, err := s.Db.InsertOne(context.TODO(), vacancy)
	if err != nil {
		return err
	}
	fmt.Println("Inserted a single document: ", insertResult.InsertedID)
	return nil
}

func (s *StorageMongo) GetBiId(id int) (model.Vacancy, error) {
	var result model.Vacancy
	err := s.Db.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&result)
	if err != nil {
		return model.Vacancy{}, err
	}
	fmt.Println("Found a single document: ", result)

	return result, nil
}

func (s *StorageMongo) GetList() ([]model.Vacancy, error) {
	findOptions := options.Find()
	var users []model.Vacancy
	cur, err := s.Db.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		log.Fatal(err)
	}
	for cur.Next(context.TODO()) {
		var elem model.Vacancy
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		users = append(users, elem)
	}

	fmt.Println("list users: ", users)
	return users, err
}

func (s *StorageMongo) Delete(id int) error {
	deleteResult, err := s.Db.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil {
		return err
	}
	fmt.Println("Deleted count: ", deleteResult.DeletedCount)
	return nil
}

func NewMongo() *StorageMongo {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:27017", "127.0.0.1")))
	if err != nil {
		return nil
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil
	}
	collection := client.Database("test").Collection("trainers")

	return &StorageMongo{Db: collection}

}
