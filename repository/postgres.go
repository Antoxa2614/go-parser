package repository

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/antoxa2614/go-parser/model"
	"log"
)

type dbPostgres struct {
	data *sql.DB
}

var CreateTable string = `
CREATE TABLE IF NOT EXIST vacancy
(
     id_postgres SERIAL PRIMARY KEY,
     id INTEGER UNIQUE
     description VARCHAR
     context VARCHAR,
     type VARCHAR,
     dataPosted VARCHAR,
     title VARCHAR,
     );
`

func NewPostgres() *dbPostgres {
	connStr := "user=antoxa password=rewq1234 host=postgresdb port=5432 dbname=habrdb sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(CreateTable)
	if err != nil {
		log.Fatal(err)
	}

	return &dbPostgres{
		data: db,
	}
}

func (d dbPostgres) Create(vac model.Vacancy) error {
	_, err := d.data.Exec("INSERT INTO vacancy (id,description,context,type,dataPosted,title) VALUES ($1,$2,$3,$4,$5,$6)", vac.ID, vac.Description, vac.Context, vac.Type, vac.DatePosted, vac.Title)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

func (d dbPostgres) GetBiId(id int) (model.Vacancy, error) {
	var vacancy model.Vacancy
	row := d.data.QueryRow("SELECT id,description,context,type,dataPosted,title FROM vacancy WHERE id=$1", id)
	err := row.Scan(&vacancy.ID, &vacancy.Description, &vacancy.Context, &vacancy.Type, &vacancy.DatePosted, vacancy.Title)
	if err != nil {
		return model.Vacancy{}, err
	}
	return vacancy, nil

}

func (d dbPostgres) GetList() ([]model.Vacancy, error) {
	vacancies := []model.Vacancy{}
	rows, err := d.data.Query("SELECT *FROM vacancy")
	if err != nil {
		return []model.Vacancy{}, err
	}
	defer rows.Close()

	for rows.Next() {
		vacancy := model.Vacancy{}
		err := rows.Scan(&vacancy.ID, &vacancy.Description, &vacancy.Context, &vacancy.Type, &vacancy.DatePosted, vacancy.Title)
		if err != nil {
			return []model.Vacancy{}, err
		}
		vacancies = append(vacancies, vacancy)
	}
	return vacancies, nil

}

func (d dbPostgres) Delete(id int) error {
	_, err := d.data.Exec("DELETE FROM vacancy WHERE id=$1", id)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}
