package selenium

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"gitlab.com/antoxa2614/go-parser/model"
	"gitlab.com/antoxa2614/go-parser/servise"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func Parsing(data *servise.VacancerServise) {
	time.Sleep(5 * time.Second)
	log.Println("Начинаем парсинг")
	caps := selenium.Capabilities{"browserName": "firefox"}
	var wd selenium.WebDriver // Создаем новый драйвер с заданными настройками
	var err error

	for i := 0; i < 5; i++ {
		wd, err = selenium.NewRemote(caps, "http://seleniumFirefox:4444/wd/hub") // Соединяемся с драйвером
		if err == nil {
			log.Println("Успешное подключение")
			break
		}
		log.Printf("Ошибка подключения к драйверу: %v", err)
		time.Sleep(5 * time.Second)
	}

	if wd == nil {
		log.Fatalf("Не удалось подключиться к удаленному драйверу после 5 попыток")
	}
	defer wd.Quit()

	log.Println("Начало GET")
	wd.Get("https://career.habr.com/vacancies?page=1&q=golang&type=all")

	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		log.Printf("Не удалось найти элемент: %s\n", err)
		return
	}
	vacancyCountRow, err := elem.Text()
	if err != nil {
		log.Fatalln(err)
	}

	vacancyCount, err := strconv.Atoi(strings.Fields(vacancyCountRow)[1])

	countOfPage := vacancyCount / 25
	if countOfPage%25 > 0 {
		countOfPage++
	}

	list := make([]model.Vacancy, vacancyCount)
	var countVacancy, errUnmarhal int
	for i := 1; i <= countOfPage; i++ {
		query := "golang"
		wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", i, query))

		elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			log.Fatalln(err)
		}

		for i := range elems {
			link, err := elems[i].GetAttribute("href")
			log.Printf("Вакансия найдена: %s\n", link)
			if err != nil {
				continue
			}

			resp, err := http.Get("https://career.habr.com" + link)
			if err != nil {
				log.Println(err)
			}
			var doc *goquery.Document
			doc, err = goquery.NewDocumentFromReader(resp.Body)
			if err != nil && doc != nil {
				log.Println(err)
			}
			dd := doc.Find("script[type=\"application/ld+json\"]")
			if dd == nil {
				log.Fatalln("habr vacancy nodes not found")
			}

			var vacancy model.Vacancy
			b := bytes.NewBufferString(dd.First().Text())
			err = json.Unmarshal(b.Bytes(), &vacancy)
			if err != nil {
				log.Println(err)
				errUnmarhal++
			}

			list = append(list, vacancy)

			countVacancy++
		}

	}
	for i, vac := range list {
		vac.ID = strconv.Itoa(i)
		data.Create(vac)
	}

	fmt.Printf("Количество вакансий: %d\nКоличество ошибок: %d\n", countVacancy, errUnmarhal)
	log.Println("Парсинг окончен")

}
