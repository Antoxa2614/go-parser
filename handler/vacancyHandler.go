package handler

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/antoxa2614/go-parser/model"
	"net/http"
	"strconv"
)

type VakancyServiser interface {
	Create(vac model.Vacancy) error
	GetBiId(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type VakancerHandler struct {
	data VakancyServiser
}
type Message struct {
	message string `json:"message"`
	code    int    `json:"code"`
}

func NewVacancerHandler(data VakancyServiser) *VakancerHandler {
	return &VakancerHandler{data: data}
}
func (h *VakancerHandler) Routes() chi.Router {
	router := chi.NewRouter()
	router.Post("/", h.CreateVacancy)
	router.Get("/{vacanciId}", h.GetVacancyById)
	router.Delete("/{vacancyId}", h.DeleteVacancy)
	router.Get("/getList", h.GetListVacancy)
	return router

}

func (v *VakancerHandler) CreateVacancy(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var vacancy model.Vacancy
	err := json.NewDecoder(request.Body).Decode(&vacancy)
	if err != nil {
		msq := Message{
			message: "Bad request ",
			code:    400,
		}
		http.Error(writer, msq.message, msq.code)
		return

	}
	err = v.data.Create(vacancy)
	if err != nil {
		msq := Message{
			message: "Bad request ",
			code:    400,
		}
		http.Error(writer, msq.message, msq.code)
		return

	}

	writer.WriteHeader(http.StatusOK)
	json.NewEncoder(writer).Encode(vacancy)
}
func (v *VakancerHandler) GetVacancyById(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	param := chi.URLParam(request, "vacancyId")
	id, err := strconv.Atoi(param)
	if err != nil {
		msq := Message{
			message: "Bad request ",
			code:    400,
		}
		http.Error(writer, msq.message, msq.code)
		return

	}
	vacancy, err := v.data.GetBiId(id)
	if err != nil {
		msq := Message{
			message: "Not Found ",
			code:    404,
		}
		http.Error(writer, msq.message, msq.code)
		return

	}
	writer.WriteHeader(http.StatusOK)
	json.NewEncoder(writer).Encode(vacancy)
}
func (v *VakancerHandler) DeleteVacancy(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	param := chi.URLParam(request, "vacancyId")
	id, err := strconv.Atoi(param)
	if err != nil {
		msq := Message{
			message: "Bad request ",
			code:    400,
		}
		http.Error(writer, msq.message, msq.code)
		return
	}
	err = v.data.Delete(id)
	if err != nil {
		msq := Message{
			message: "Bad request ",
			code:    400,
		}
		http.Error(writer, msq.message, msq.code)
		return
	}
	json.NewEncoder(writer).Encode("Succes")
}
func (v *VakancerHandler) GetListVacancy(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")
	var vacansyes []model.Vacancy
	vacansyes, err := v.data.GetList()
	if err != nil {
		msq := Message{
			message: "Bad request ",
			code:    400,
		}
		http.Error(writer, msq.message, msq.code)
		return
	}
	writer.WriteHeader(http.StatusOK)
	json.NewEncoder(writer).Encode(vacansyes)
}
