package db

import (
	"fmt"
	"gitlab.com/antoxa2614/go-parser/repository"
	"gitlab.com/antoxa2614/go-parser/servise"
)

type DBController struct { // Pet контроллер
	servise.Vacancer
}

func NewDBController(nameDatabase string) *DBController { // конструктор нашего контроллера

	switch nameDatabase {
	case "postgres":
		fmt.Println("Старт базы Postgre")
		return &DBController{repository.NewPostgres()}
	case "mongo":
		fmt.Println("Старт базы Mongo")
		return &DBController{repository.NewMongo()}
	default:
		fmt.Println("Старт базы на локальном сервере")
		return &DBController{repository.NewVacanStorage()}
	}

}
