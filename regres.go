package main

import "gitlab.com/antoxa2614/go-parser/model"

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /search vacancy vacancySearchRequest
// Создание вакансии.
// responses:
//   200: vacancySearchResponse

// swagger:parameters vacancySearchRequest
type vacancySearchRequest struct {
	// in:body
	Body model.Vacancy
}

// swagger:response vacancySearchResponse
type vacancySearchResponse struct {
	// in:body
	Body model.Vacancy
}

// swagger:route POST /get vacancy vacancyIdGetRequest
// Поиск вакансии по ID.
// responses:
//   200: vacancyIdGetResponse

// swagger:parameters vacancyIdGetRequest
type vacancyIdGetRequest struct {
	// in:body
	VacancyId string `json:"vacancyId"`
}

// swagger:response vacancyIdGetResponse
type vacancyIdGetResponse struct {
	// in:body
	Body model.Vacancy
}

// swagger:route DELETE /delete vacancy vacancyDeleteRequest
// Удалить вакансию.
// responses:
//   200: vacancyDeleteResponse

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// in:body
	VacancyId string `json:"vacancyId"`
}

// swagger:response vacancyDeleteResponse
type vacancyDeleteResponse struct {
	// in:body
	Body model.Vacancy
}

// swagger:route GET /list vacancy vacancyListRequest
// Список всех вакансий.
// responses:
//   200: vacancyListResponse

// swagger:parameters vacancyListRequest

// swagger:response vacancyListResponse
type vacancyListResponse struct {
	// in:body
	Body []model.Vacancy
}
