package model

type Vacancy struct {
	ID          string `json:"id"`
	Context     string `json:"@context"`
	Type        string `json:"@type"`
	DatePosted  string `json:"datePosted"`
	Title       string `json:"title"`
	Description string `json:"description"`
}
