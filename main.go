package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/joho/godotenv"
	"gitlab.com/antoxa2614/go-parser/db"
	"gitlab.com/antoxa2614/go-parser/handler"
	"gitlab.com/antoxa2614/go-parser/selenium"
	"gitlab.com/antoxa2614/go-parser/servise"
	"golang.org/x/net/context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	port := ":8080"
	router := chi.NewRouter()
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
	nameDatabase := os.Getenv("nameDatabase")
	data := db.NewDBController(nameDatabase)
	vacanServise := servise.NewVacancerStorage(data)
	vacanHandler := handler.NewVacancerHandler(vacanServise)
	swaggerHundler := handler.NewSwaggerHandler()
	time.Sleep(5 * time.Second)
	go selenium.Parsing(vacanServise)

	VacancyRouter := vacanHandler.Routes()
	SwaggerRouter := swaggerHundler.Routes()

	router.Use(middleware.Logger)
	router.Mount("/", VacancyRouter)
	router.Mount("/swagger", SwaggerRouter)
	srv := http.Server{Addr: port, Handler: router}
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
