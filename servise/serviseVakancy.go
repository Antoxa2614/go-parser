package servise

import "gitlab.com/antoxa2614/go-parser/model"

type Vacancer interface {
	Create(vac model.Vacancy) error
	GetBiId(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
}

type VacancerServise struct {
	repo Vacancer
}

func NewVacancerStorage(repo Vacancer) *VacancerServise {
	return &VacancerServise{repo: repo}

}

func (s *VacancerServise) Create(vac model.Vacancy) error {
	return s.repo.Create(vac)
}

func (s *VacancerServise) GetBiId(id int) (model.Vacancy, error) {
	return s.repo.GetBiId(id)
}

func (s *VacancerServise) GetList() ([]model.Vacancy, error) {
	return s.repo.GetList()
}

func (s *VacancerServise) Delete(id int) error {
	return s.repo.Delete(id)
}
